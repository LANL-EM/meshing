TA-16 V1 Mesh A 40m, no wells
web pages https://meshing.lanl.gov/proj/ER_LANL_TA-16/images_V1_40m/mesh.html 
and copied locally into web_pages directory

========================================================================
About this repo:

Summary of sizes with total at end:
     du -ch RDXVA2016 > RDXVA2016_dirs.txt
Summary list of every file in the archive: 
     du -La RDXVA2016 > RDXVA2016_files.txt

Arhive of directories and files:

 239M  FEHM_DIR.tar.gz
 578M  MESH_DIRS.tar.gz

Use this to extract directory from compressed tar file:
  tar -zxvf [name].tar.gz 

This was used to archive and compress directories:
  tar -zhcvf FEHM_DIR.tar.gz fehm_TA16_V1_40m
  tar -zhcvf MESH_DIRS.tar.gz hex_polyA_V1_40m 2D_polyA_V1.0 TA16_data data

========================================================================
FEHM MESH STATS

Stacked mesh with smooth layers at
Watertable, Tpf3 top, Qbof top, and ground surface

number of nodes =       60516        
number of elements =   345050

Voronoi Volume min =   1.8025757E+03
Voronoi Volume max =   8.7190155E+04
Total Voronoi Volume:  1.2840455E+09

Element Volume min =   8.0469333E-03  
Element Volume max =   1.7046876E+04   

Material zones Using WC15c/TA-16.seq
Material Qbt4     25 has       761 nodes. #nodes/nnodes is   0.125751867890E-01 
Material Qbt3t    24 has      1006 nodes. #nodes/nnodes is   0.166237019002E-01 
Material Qbt3     23 has      2061 nodes. #nodes/nnodes is   0.340571105480E-01 
Material Qbt2     22 has      1747 nodes. #nodes/nnodes is   0.288683976978E-01 
Material Qbt1vu   21 has       650 nodes. #nodes/nnodes is   0.107409609482E-01 
Material Qbt1g    19 has       813 nodes. #nodes/nnodes is   0.134344631806E-01 
Material Qct      17 has      1818 nodes. #nodes/nnodes is   0.300416424870E-01 
Material Qbof     16 has      7380 nodes. #nodes/nnodes is   0.121951222420     
Material Qbof_G3  28 has      4438 nodes. #nodes/nnodes is   0.733359754086E-01 
Material Qbof_G2  27 has      4418 nodes. #nodes/nnodes is   0.730054825544E-01 
Material Tpf3     14 has     26705 nodes. #nodes/nnodes is   0.441288262606     
Material Tvt2     11 has       144 nodes. #nodes/nnodes is   0.237953593023E-02 
Material Tpf2     10 has      8575 nodes. #nodes/nnodes is   0.141698062420     

Material zones
  tet_ev_material.zone

Mesh boundary zones
  out_top.zonn out_bottom.zonn out_east_right.zonn out_north_back.zonn
  out_north_east.zonn out_south_front.zonn out_west_left.zonn

Canyon groundwater infiltration zone
  canyon_bot.zonn

Watertable and boundary watertable zones
  wtr_blw.zonn wtr_abv.zonn
  wtr_blw_bot.zonn wtr_blw_east.zonn wtr_blw_ne.zonn
  wtr_blw_north.zonn wtr_blw_south.zonn wtr_blw_west.zonn

Perched water zones (upper and lower)
  wtr_UPZ2_in.zonn wtr_UPZ2_blw.zonn
  wtr_LPZ1_in.zonn wtr_LPZ1_blw.zonn

Extended Upper Perched zone to north west boundary
  wtr_UPZ2_in_addNW2.zonn wtr_UPZ2_blw_addNW2.zonn

Tpf3s = Material 14 Tpf3 Upper Puye in the UPZ2 zone
  wtr_UPZ2_Tpf3s.zonn 

Tpf3s UPZ2 zone window, west of CdV16-1(i)
  wtr_UPZ2_Tpf3s_intr.zonn wtr_UPZ2_Tpf3s_win.zonn



